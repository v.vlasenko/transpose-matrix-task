package com.epam.rd.autotasks.matrices;
import java.util.Arrays;

public class TransposeMatrix {
    public static int[][] transpose(int[][] matrix) {

        var row = matrix.length;
        var col = matrix[0].length;
        var transposed = new int[col][row];

        for (var i = 0; i < row; i++)
            for (var j = 0; j < col; j++)
                transposed[j][i] = matrix[i][j];

        return transposed;
    }

    public static void main(String[] args) {

        System.out.println("Test your code here!\n");

        // Get a result of your code

        int[][] matrix = {
                {-4, -65, 56},
                {78, -13, 32},
                {11, -12, 77}};

        int[][] result = transpose(matrix);
        System.out.println(Arrays.deepToString(result).replace("],", "]\n"));
    }

}
